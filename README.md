# fHelper-MQTT

April 2019

Public version of the project fHelper using MQTT.
fHelper stands for Flower Helper, a project designed to report via email and slack when a flower in the office needs to be watered.
This specific version of fHelper uses the MQTT protocol also to collect data and follow the plant needs in graphs.
The project also report the ambient temperature and humidity.

It is based on a Wroom32 processor for the client (intitially the Huzzah32 board from Adafruit) and a Node-Red flow running over a Raspberry Pi 3 as server.
Public means that some private information like WiFi SSID/Pwd/internal address are omitted and thus whoever use this code has to complete it.

Some related articles are available on my blog :

- https://hanixdiy.blogspot.com/2019/04/fhelper-mqtt-edition.html
- https://hanixdiy.blogspot.com/2019/03/set-up-huzzah32-iot-client-testing.html
- https://hanixdiy.blogspot.com/2019/03/set-up-raspberry-iot-server.html
- https://hanixdiy.blogspot.com/2019/03/fhelper-on-node-red.html